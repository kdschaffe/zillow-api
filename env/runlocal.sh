#!/usr/bin/env sh

# Defaults
CLEAN=false

# Arguments
while [ "$#" -gt 0 ]; do
  case "$1" in
		# Allow for multiple formats
		# #  eg. -c Flags
		-c) CLEAN=true; shift 1;;
		# eg. -clean arg
    -clean) CLEAN="$2"; shift 2;;
		#	eg. --clean=arg
    --clean=*) CLEAN="${1#*=}"; shift 1;;
    #--env|--bulk) echo "$1 requires an argument" >&2; exit 1;;
    -user) LOCAL_USER="$2"; shift 1;;
    --user=*) LOCAL_USER="${1#*=}"; shift 1;;

		# Unaccepted options
    -*) echo "unknown option: $1" >&2; exit 1;;
    *) handle_argument "$1"; shift 1;;
  esac
done

# Clean
if [ $CLEAN == true ]; then
  # Remove all files to be refreshed
  echo "Removing .env"
  rm env/.env
fi

appName=$(node -pe 'require("./package.json").name')

# DOWNLOAD ENVIRONMENT FROM CLOUD
# ...
# AND WRITE TO .ENV
# ...
# Local env variables are loaded with the dotenv[-safe] npm packages, via .env and .env.example files
# NOTE: This only works with 1-line env variables, so add them manually here, after the file is created
# if [ ! -f ./env/.env ]; then
#   echo ".env not found. Downloading..."
#   touch ./env/.env
#   while read p; do
#     env_var=$(echo $p | cut -d= -f1)
#     variable=$(cf env zillow-api | grep $env_var |  sed s/$env_var:// | tr -d '[:space:]')
#     echo $env_var=$variable >> ./env/.env
#   done <./env/.env.example
#   echo 'Downoad complete - .env was created'
# fi

# Ghosting
if [ "$LOCAL_USER" == "" ]; then
  LOCAL_USER=$(whoami)
fi

# Run App
LOCAL_USER=$LOCAL_USER node_modules/nodemon/bin/nodemon.js -i '*.test.js' -r dotenv-safe/config . dotenv_config_path=./env/.env dotenv_config_sample=./env/.env.example
