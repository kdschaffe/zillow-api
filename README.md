# Fun Zillow API

## What

This is a simple wrapper around the `GetRegionChildren` Zillow API.

See https://www.zillow.com/howto/api/GetRegionChildren.htm

## Run

First, run `npm install`

To run, you need a Zillow Web Service Identifier. Add it to the `/env/.env` file by replacing <ZWSID> with your ID.

Then, run with `npm run serve`

## Examples

NOTE: Adjust host/port if needed (see console).

### Search by city, state, and childtype, default response in xml

`GET localhost:6001/regions?state=wa&city=seattle&childtype=neighborhood`

### Search by regionId and parse response to json

`GET localhost:6001/regions?regionId=16037&json=true`
