import { Router } from 'express';
import regions from './regions';

const router = new Router();

router.use('/regions', regions);

export default router;
