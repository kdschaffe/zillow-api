import Promise from 'bluebird';
import { errors } from '../../utils';
import { ZWSID, ZILLOW_REGIONS_API } from '../../services/env';

const request = Promise.promisifyAll(require('request'));

const { GatewayTimeoutError } = errors.types;

const getRegions = async (
  regionId,
  state,
  county,
  city,
  childtype,
) => {
  try {
    const options = {
      url: `${ZILLOW_REGIONS_API}`,
      qs: {
        'zws-id': ZWSID,
        // if params exist, pass them
        ...regionId && { regionId },
        ...state && { state },
        ...county && { county },
        ...city && { city },
        ...childtype && { childtype },
      },
      strictSSL: false,
    };

    const response = await request.getAsync(options);
    const payload = response.body;

    // We may want to do some custom error handling to wrap zillow api errors

    return payload;
  } catch (e) {
    throw new GatewayTimeoutError(e.message);
  }
};

export default {
  getRegions,
};
export { getRegions };
