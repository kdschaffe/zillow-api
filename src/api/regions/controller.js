import parser from 'xml2json';
import { response, jsonViews as views } from '../../utils';
import { getRegions } from './model';

const getAll = ({ query }, res, next) => {
  const {
    regionId, state, county, city, childtype, json,
  } = query;

  getRegions(regionId, state, county, city, childtype)
    .then((regionsXML) => {
      if (json) {
        const options = {
          object: false,
          reversible: false,
          coerce: false,
          sanitize: true,
          trim: true,
          arrayNotation: false,
          alternateTextNode: false,
        };

        return parser.toJson(regionsXML, options);
      }

      return regionsXML;
    })
    .then(entity => views.found({ entity }))
    .then(({ view }) => response.success(res, 200)(view))
    .catch(error => next(error)); // delegate to global error handler
};

export default {
  getAll,
};
export {
  getAll,
};
