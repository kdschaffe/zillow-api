import { Router } from 'express';
import expressValidator from 'express-validator';
import { validation, errors } from '../../utils';
import { getAll } from './controller';

const { InputValidationError } = errors.types;

const router = new Router();

router.use(expressValidator({
  customValidators: {
    isValidChildType(value) {
      return (
        value === 'state'
        || value === 'county'
        || value === 'city'
        || value === 'zipcode'
        || value === 'neighborhood'
      );
    },
  },
}));

router.get('/',
  validation((req) => {
    req.checkQuery('regionId', 'regionId should be numeric')
      .optional()
      .isInt();
    req.checkQuery('state', 'state should be alpha')
      .optional()
      .isAlpha();
    req.checkQuery('county', 'county should be alpha')
      .optional()
      .isAlpha();
    req.checkQuery('city', 'city should be alpha')
      .optional()
      .isAlpha();
    req.checkQuery('childtype', 'childtype should be state, county, city, zipcode, or neighborhood')
      .optional()
      .isValidChildType();
    req.checkQuery('json', 'json should be true or false')
      .optional()
      .isBoolean();
  }),
  ({ query }, res, next) => {
    const {
      regionId,
      state,
    } = query;

    if (regionId || state) {
      next();
    } else {
      next(new InputValidationError(
        'regionId or state is required',
      ));
    }
  },
  getAll);

export default router;
