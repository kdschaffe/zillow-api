import winston from 'winston';
import { isLocal, port, host } from './services/env';
import express from './services/express';
import api from './api';

const http = require('http');

const app = express(api);
const server = http.createServer(app);

setImmediate(() => {
  server.listen(port, host, () => {
    const protocol = isLocal ? 'http' : 'https';
    winston.info(`Express server listening on ${protocol}://${host}:${port}`);
  });
});

export default app;
