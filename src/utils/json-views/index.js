/**
 * The idea with views, is to standarize what
 * our json responses should look like. Each REST method will likely have a similar shape, so
 * this should eliminate having to decide, guess, or lookup up how everyone else is doing it
 * and keep nice, consistent API reponses.
 *
 * HOW TO CREATE A NEW VIEW
 *   - Must take an object of shape { entity, view } as the first param.
 *   - Must return an object of shape { entity, view }, where entity is the orginal, unmodified entity and
 *     view is the new, modified, or udpdated JSON object to be sent with response or passed to the next view.
 *
 * NOTE: These views are tied to what the mysql2/db decides to return. Making this more generic
 * or making different versions for different tech might be a future goal.
 */
const _ = require('lodash');
const errors = require('../errors/types');

const getBaseView = (view) => {
  const baseView = {
    metadata: {},
    data: {}, // Can also be set to an array if a view wants to overwrite the base.
  };

  // Check that the current view is a valid baseView
  // _.has doesn't support multiple keys so this is fancy lodash way of doing it.
  if (view && _.every(_.keys(baseView), _.partial(_.has, view))) {
    return _.cloneDeep(view);
  }

  return JSON.parse(JSON.stringify(baseView));
};

const created = ({ entity, view }) => {
  const newView = getBaseView(view);

  try {
    const metadata = entity[0] || entity;
    newView.metadata.insertId = metadata.insertId;
    newView.metadata.affectedRows = metadata.affectedRows;
  } catch (error) {
    throw new errors.FailedDependencyError(error);
  }

  return {
    entity,
    view: newView,
  };
};

const found = ({ entity, view }) => {
  const newView = getBaseView(view);

  try {
    newView.data = entity;
  } catch (error) {
    throw new errors.FailedDependencyError(error);
  }

  return {
    entity,
    view: newView,
  };
};

const updatedAll = affectedRows => ({ entity, view }) => {
  const newView = getBaseView(view);

  try {
    newView.metadata.affectedRows = affectedRows || entity.affectedRows;
  } catch (error) {
    throw new errors.FailedDependencyError(error);
  }

  return {
    entity,
    view: newView,
  };
};

// Passing everything pagination needs so we don't need to assume anything exists on { entity, view }.
const pagination = (page, pageSize, totalRecords) => ({ entity, view }) => {
  const newView = getBaseView(view);

  try {
    newView.metadata.pagination = {};
    newView.metadata.pagination.totalRecords = totalRecords;
    newView.metadata.pagination.records = pageSize;
    newView.metadata.pagination.page = page;
    newView.metadata.pagination.pageSize = pageSize;
    newView.metadata.pagination.totalPages = Math.ceil(totalRecords / pageSize);
  } catch (error) {
    throw new errors.FailedDependencyError(error);
  }

  return {
    entity,
    view: newView,
  };
};

const totals = totalData => ({ entity, view }) => {
  const newView = getBaseView(view);

  try {
    newView.totals = totalData;
  } catch (error) {
    throw new errors.FailedDependencyError(error);
  }

  return {
    entity,
    view: newView,
  };
};

const performance = executiontime => ({ entity, view }) => {
  const newView = getBaseView(view);

  try {
    newView.metadata.executiontime = executiontime;
  } catch (error) {
    throw new errors.FailedDependencyError(error);
  }

  return {
    entity,
    view: newView,
  };
};

module.exports = {
  created,
  found,
  pagination,
  updatedAll,
  totals,
  performance,
};
