/*
 * The type class variable is being used to overcome limitations of babel. I found other solutions but this way we're still
 * using normal error classes and don't need a babel transform.
 * See - http://stackoverflow.com/questions/33870684/why-doesnt-instanceof-work-on-instances-of-error-subclasses-under-babel-node
 */

/*
 * The errors class variable is a way to use one error response to report mulitple errors. An example is an InputValidationError
 * that multiple bad parameters.
 */

/**
 * Represents a not found error when searching for data.
 */
class NotFoundError extends Error {
  constructor(message, errors = []) {
    super(message);
    this.type = 'NotFoundError';
    this.status = 404;
    this.errors = errors;
  }
}

/**
 * Represents a bad request.
 */
class BadRequestError extends Error {
  constructor(message, errors) {
    super(message);
    this.type = 'BadRequestError';
    this.status = 400;
    this.errors = errors;
  }
}

/**
 * Represents a bad request payload.
 */
class BadRequestPayloadError extends Error {
  constructor(message, errors) {
    super(message);
    this.type = 'BadRequestPayloadError';
    this.status = 422;
    this.errors = errors;
  }
}

/**
 * Represents an input validation error.
 */
class InputValidationError extends Error {
  constructor(message, errors) {
    super(message);
    this.type = 'InputValidationError';
    this.status = 422;
    this.errors = errors;
  }
}

/**
* Represents a failed dependency error.
*/
class FailedDependencyError extends Error {
  constructor(message, errors) {
    super(message);
    this.type = 'FailedDependencyError';
    this.status = 424;
    this.errors = errors;
  }
}

class InternalServerError extends Error {
  constructor(message, errors = []) {
    super(message);
    this.type = 'InternalServerError';
    this.status = 500;
    this.errors = errors;
  }
}

/**
 * Good for when an external resource fails.
 */
class GatewayTimeoutError extends Error {
  constructor(message, errors = []) {
    super(message);
    this.type = 'GatewayTimeoutError';
    this.status = 504;
    this.errors = errors;
  }
}

module.exports = {
  NotFoundError,
  BadRequestPayloadError,
  BadRequestError,
  InputValidationError,
  FailedDependencyError,
  InternalServerError,
  GatewayTimeoutError,
};
