class errorResponse {
  constructor(message, errors = []) {
    this.error = {
      message,
      errors,
    };
  }
}

module.exports = errorResponse;
