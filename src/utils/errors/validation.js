/**
 * Usage:
 *  See: https://www.npmjs.com/package/express-validator
 *
 *  router.get('/',
 *    validation((req, res, next) => { // eslint-disable-line no-unused-vars
 *      // Same as express-validator
 *      req.checkQuery('search', 'search param required').notEmpty();
 *      req.checkQuery('zip', 'zip param required').notEmpty();
 *    }),
 *    index);
 *
 */
const errors = require('./types');

function validationHandler(req, res, next) {
  req.getValidationResult().then((result) => {
    if (!result.isEmpty()) {
      // We may want a switch here to check for different types of errors
      // so we can throw different error types. E.g. A missing param might
      // throw a BadRequestError.
      next(new errors.InputValidationError(
        'There was an error validating the request.',
        result.array()
      ));
    }

    next();
  });
}

function runValidation(validationFunction) {
  return function runValidationMiddleware(req, res, next) {
    validationFunction(req, res, next);
    next();
  };
}

function validation(validationFunction) {
  // Return two middleware that run sequentially.
  return [
    runValidation(validationFunction),
    validationHandler,
  ];
}

module.exports = validation;
