const winston = require('winston');
const _ = require('lodash');
const errorTypes = require('./types');
const ErrorResponse = require('./response');
const validation = require('./validation');

/**
 * A generic global express exception error handler. This method should be the
 * last app.use() method added to your express app. It will catch all uncaught
 * exceptions that are created by code that is called from your express app.
 *
 * This method will respond with an appropriate HTTP status code based on
 * the error class and message.
 *
 * It will also log the exception message and in the event of an HTTP 500, the
 * stacktrace as well.
 *
 * APIs should use next(error) in catch blocks.
 *
 * @param  {object} err an instance of Error thrown somewhere in the express app
 * @param  {object} req the express request object
 * @param  {object} res the express response object
 * @param  {function} next the next express middleware (should be null!)
 */
function globalExceptionHandler(err, req, res, next) { // eslint-disable-line no-unused-vars
  winston.error(`${err.message}`);

  // If not a custom error type.
  if (_.has(err, 'status')) {
    res.status(err.status).json(
      new ErrorResponse(err.message, err.errors),
    );
  } else {
    winston.error('STACK:', err.stack);
    res.status(500).json(
      new ErrorResponse(err.message),
    );
  }
}

/**
 * A generic 404 not found error handler. This method should be the first method
 * that you app.use() after all of the routes have been defined.
 *
 * @param  {object} req the express request object
 * @param  {object} res the express response object
 * @param  {function} next the next express middleware (should be null!)
 */
function notFoundHandler(req, res, next) { // eslint-disable-line no-unused-vars
  res.status(404).json(
    new ErrorResponse('The requested resource could not be found.'),
  );
}

module.exports = {
  globalExceptionHandler,
  notFoundHandler,
  validation,
  types: errorTypes,
};
