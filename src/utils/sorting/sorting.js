import _ from 'lodash';

// Return cloned updated record
const addToEnd = (
  collection,
  ref,
  options = {
    idField: 'id',
    sortField: 'order',
  },
) => {
  const { idField, sortField } = options;

  // Find max item
  const maxItem = _.maxBy(collection, sortField);
  const max = maxItem ? maxItem[sortField] : 0; // 0 if no records with an order are found

  // Find ref object
  let refObj = null;
  if (_.isNumber(ref)) {
    refObj = _(collection)
      .chain()
      .find([idField, ref])
      .cloneDeep()
      .value();
  } else {
    refObj = _.cloneDeep(ref);
  }

  // Set the order
  refObj[sortField] = max + 1;

  return refObj;
};

const calculateSortOptions = (
  collection,
  options = {
    sortField: 'order',
    groupField: 'group',
    sortOptionsField: 'options',
  },
) => {
  const { sortField, groupField, sortOptionsField } = options;

  // Get unique groupField values
  const groupValues = _(collection)
    .uniqWith((arrVal, othVal) => arrVal[groupField] === othVal[groupField])
    .map(groupField)
    .orderBy(null, ['desc'])
    // .orderBy()
    .value();

  // Get items in collection for each group.
  groupValues.forEach((groupValue) => {
    // Get group
    const group = _.filter(collection, item => item[groupField] === groupValue);
    // Get list of options
    const sortOptions = _(group)
      .omitBy(item => !item[sortField])
      .map(item => ({
        label: item[sortField],
        value: item[sortField],
      }))
      .value();
    // Set the options
    _.forEach(group, (item) => { item[sortOptionsField] = sortOptions; }); // eslint-disable-line
  });

  return collection;
};

export { addToEnd };
export { calculateSortOptions };
