const _ = require('lodash');
const util = require('./util');

const areDefined = util.areDefined; // eslint-disable-line
const arePositiveIntegers = util.arePositiveIntegers; // eslint-disable-line

/**
 * Builds the appropriate function to be consumed by Express as a middleware
 * @param  {Object} options options for configuring the pagination
 * @return {Function}         a function that is the Express Middleware
 */
function middlewareWrapper(
  options = {
    shouldError: true,
  },
) {
  /**
   * Inspects the req.query object and injects a pagination object into the req
   * @param  {Object} req the Express Request object
   * @param  {Object} res the Express Response object
   * @param  {Function} next the 'next' Express Middleware
   */
  const middleware = function pagination(_req, res, next) {
    const req = _req; // Appeasing no-param-reassign rule

    // Inspect req.query for pagination query parameters
    const { page, pageSize } = req.query;

    // each function call will return a boolean that is false if the validation
    // check fails. If one function returns false, it will fail early and skip
    // to the next() call. At the end of all calls, they should be integers,
    // and are thus saved to the pagination object in the request
    if (areDefined(page, pageSize) && arePositiveIntegers(page, pageSize)) {
      req.pagination = {
        pageSize: _.toInteger(pageSize),
        page: _.toInteger(page),
      };
      next();
    } else if (areDefined(page, pageSize) && options.shouldError) {
      res.send(400).json({ error: 'Invalid pagination parameters' });
    } else {
      // This will hit if they were never defined or o.shouldError is false
      next();
    }
  };

  return middleware;
}

module.exports = middlewareWrapper;
