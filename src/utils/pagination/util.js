const _ = require('lodash');

const util = {
  areDefined(page, pageSize) {
    return (!_.isUndefined(page) && !_.isUndefined(pageSize));
  },

  arePositiveIntegers(_page, _pageSize) {
    const isNumber = /^\+?(0|[1-9]\d*)$/;
    const radix = 10;
    const pageSize = _.parseInt(_pageSize);
    const page = parseInt(_page, radix);
    return (isNumber.test(page) && isNumber.test(pageSize));
  },

  paginateCollection(_collection, pagination) {
    let collection = _collection;
    const total = collection.length;

    if (pagination.pageSize && pagination.page) {
      collection = _.slice(collection,
        (pagination.page - 1) * pagination.pageSize, // start
        ((pagination.page - 1) * pagination.pageSize) + pagination.pageSize); // end

      return {
        docs: collection,
        total,
        limit: pagination.pageSize,
        page: pagination.page,
        pages: Math.ceil(total / pagination.pageSize),
      };
    }

    return collection;
  },

};

module.exports = util;
