/**
 * This is an alternative to node-uuid for generating unique ID's that are in decimal and integer format (no hyphens).
 *
 * TLDR;
 *    uuid -> '110ec58a-a0f2-4ac4-8393-c866d813b8d1'
 *    Snowflake -> '5829986774188818434'
 *
 * Usage:
 *  import { idGen } from '../../utils';
 *  idGen.next(); // -> '5829986774188818434'
 *
 * For more info or adding other formats see Snowflake approach: https://blog.tompawlak.org/generate-unique-identifier-nodejs-javascript
 */

const intformat = require('biguint-format');
const FlakeId = require('flake-idgen');

const flakeIdGen = new FlakeId();

const generator = {
  next() {
    return intformat(flakeIdGen.next(), 'dec');
  },
};

module.exports = generator;
