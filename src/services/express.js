import _ from 'lodash';
import express from 'express';
import cors from 'cors';
import compression from 'compression';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import expressValidator from 'express-validator';

import { errors } from '../utils';
import { MORGAN_FORMAT } from './env';

const { globalExceptionHandler, notFoundHandler } = errors;

export default (routes) => {
  const app = express();

  app.use(morgan(MORGAN_FORMAT));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(cors({
    origin: true,
    credentials: true,
  }));

  app.use(compression());
  app.use(expressValidator({
    customValidators: {
      isArray(value) {
        return Array.isArray(value);
      },
    },
    customSanitizers: {
      toUpper(value) {
        return _.toUpper(value);
      },
    },
  }));
  app.use(routes);
  app.use(notFoundHandler);
  app.use(globalExceptionHandler);

  return app;
};
