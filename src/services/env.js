/* eslint-disable no-multi-assign */
import cfenv from 'cfenv';

const config = {};

const requireProcessEnv = (name) => {
  if (!process.env[name]) {
    throw new Error(`You must set the ${name} environment variable`);
  }
  return process.env[name];
};

// App Env Vars
export const MORGAN_FORMAT = config.MORGAN_FORMAT = requireProcessEnv('MORGAN_FORMAT');
export const ZILLOW_REGIONS_API = config.ZILLOW_REGIONS_API = requireProcessEnv('ZILLOW_REGIONS_API');
export const ZWSID = config.ZWSID = requireProcessEnv('ZWSID');

// App UPS Vars

// System vars
export const isLocal = config.isLocal = cfenv.getAppEnv().isLocal;
export const host = config.host = cfenv.getAppEnv().bind;
export const port = config.port = cfenv.getAppEnv().port;

export default config;
